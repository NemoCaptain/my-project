from flask import request, render_template, redirect, url_for
from flask_login import login_required, current_user
from .models import Coin, Catalog, db, UserCoin, CoinNumber
from operator import attrgetter
from more_itertools import unique_everseen
from .parser import get_all_information, get_coins_links
from collections import Counter
#### могут быть лишние int и str , print
# NemoCaptain
##  if request.method == 'POST': !!!!!!!!!!!!!!!!!

def delete(element):
    db.session.delete(element)
    db.session.commit()

def views(app):
    @app.route('/')
    def index():
        if current_user.get_id():
            return redirect(url_for('gallery'))
        else:  ####
            return render_template('index.html')

    @app.route('/profile')
    @login_required
    def profile():
        return render_template('profile.html')

    @app.route('/gallery', methods=['POST', 'GET'])
    def gallery():
        if request.method == 'POST':
            coins_link = [str(request.form.get('coin_link'))]
            if not (coins_link == ['None']) and not (coins_link == ['']): #### сделать проверку на правильность ссылки
                get_all_information(coins_link)
            else:
                print('ссылки на монету нет') #выводить оповещение о неверной ссслыке

            collection_link = str(request.form.get('collection_link'))
            if not (collection_link == 'None') and not (collection_link == ''):  #### сделать проверку на правильность ссылки
                get_all_information(get_coins_links(collection_link))            ## показывать пользователю прогресс
            else:
                print('ссылки на коллекцию нет')

        coins_list = list(unique_everseen([coin for coin in db.session.query(Coin)
                                                for user_coin_row in coin.user_coins
                                                if (user_coin_row.user_id == current_user.id) and not (coin.coin_id == 1)]))
        sort_type = str(request.form.get('sort'))
        print(sort_type)
        if sort_type == 'None':
            sort_type = 'Без сортировки'
        if sort_type == 'по возрастанию цены':  ####
            coins_list = sorted(coins_list, key=attrgetter('price'))
        elif sort_type == 'по убыванию цены':
            coins_list = sorted(coins_list, key=attrgetter('price'), reverse=True)
        elif sort_type == 'по возрастанию года':
            coins_list = sorted(coins_list, key=attrgetter('year'))
        elif sort_type == 'по убыванию года':
            coins_list = sorted(coins_list, key=attrgetter('year'), reverse=True)
        elif sort_type == 'по стране':
            coins_list = sorted(coins_list, key=attrgetter('country'))
        elif sort_type == 'по весу':
            coins_list = sorted(coins_list, key=attrgetter('weight'))  ###
        elif sort_type == 'по диаметру':  ###чекнуть английский
            coins_list = sorted(coins_list, key=attrgetter('diameter'))  ###
        elif sort_type == 'по каталожному номеру':
            coins_list = sorted(coins_list, key=attrgetter('krause_number'))
        elif sort_type == 'по периоду':
            coins_list = sorted(coins_list, key=attrgetter('period'))
        else:
            pass

        coins_list_new = []
        for coin in coins_list:
            number = db.session.query(CoinNumber).filter_by(coin_id=coin.coin_id, user_id=current_user.id).first()
            coins_list_new.append((coin, number))
        return render_template('gallery.html', coins_list=coins_list_new, sort_type=sort_type , change_number_mode=False)

    @app.route('/catalogs', methods=['POST', 'GET'])
    def catalogs(data=None):
        print('catalogS')
        catalog_name = str(request.form.get('catalog_name'))
        groups = list(
            unique_everseen(
                [catalog.group for catalog in db.session.query(Catalog) for user_coin_row in catalog.user_coins
                 if (user_coin_row.user_id == current_user.id) and not (catalog.catalog_id == 1)]))

        if request.form.get('catalog_group') is not None and request.form.get('catalog_group') is not '': ## поставить регулярные выражения
            catalog_group = str(request.form.get('catalog_group'))
        else:
            catalog_group = 'Без группы'

        names = [db.session.query(Catalog).filter(Catalog.catalog_id == row.catalog_id).first().catalog_name
                 for row in current_user.user_coins]

        if not ((catalog_name in names) and catalog_group in groups) and not (catalog_name == '') and not (catalog_name == 'None'):
            db.session.add(UserCoin(user=current_user,
                                    coin=db.session.query(Coin).filter(Coin.coin_id == 1).first(),
                                    catalog=Catalog(catalog_name=catalog_name, group = catalog_group)))
            db.session.commit()

            groups = list(
                unique_everseen(         #обновляем саисок групп тк добавили монету
                    [catalog.group for catalog in db.session.query(Catalog) for user_coin_row in catalog.user_coins
                     if (user_coin_row.user_id == current_user.id) and not (catalog.catalog_id == 1)]))
        catalog_list = list(
            unique_everseen([catalog for catalog in db.session.query(Catalog) for user_coin_row in catalog.user_coins
                             if (user_coin_row.user_id == current_user.id) and not (catalog.catalog_id == 1)]))

        data = dict(current_group=request.args.get('current_group')) ##### мб обойтись без словаря
        if data['current_group'] is not None:
            current_group = str(data['current_group'])
            catalog_list = [catalog for catalog in catalog_list if catalog.group == current_group]
        else:
            print('выбирается каталог куда записана последняя монета')
            if db.session.query(UserCoin).filter(UserCoin.user_id == current_user.id, UserCoin.catalog_id != 1).first() : #проверка на наличие на наличие не 1х каталогов
                catalog_id = (db.session.query(UserCoin).filter(UserCoin.user_id == current_user.id).first().catalog_id)
                ### нужно добавить catalogу параметр юзер и пользоваться
                current_group = (db.session.query(Catalog).filter(Catalog.catalog_id == catalog_id).first().group)
                catalog_list = [catalog for catalog in catalog_list if catalog.group == current_group]
            else:
                current_group = ''
            # выбирается каталог куда записана первая монета, хотя надо бы слелать наоборот
        try:
            i=groups.index(current_group)
            groups[0], groups[i] = groups[i], groups[0]
        except:
            print('rtwb')

        catalog_list_new = []
        for catalog in catalog_list:
            available_coins_number = 0
            all_coins_number = 0
            for coin in db.session.query(UserCoin).filter(UserCoin.user_id == current_user.id,
                                                          UserCoin.catalog_id == catalog.catalog_id,
                                                          UserCoin.coin_id != 1).all():
                try:
                    num = db.session.query(CoinNumber).filter(CoinNumber.user_id == current_user.id, CoinNumber.coin_id == coin.coin_id).first().number
                except:
                    num = 0
                all_coins_number += 1
                if num > 0:
                    available_coins_number += 1
            catalog_list_new.append((catalog, available_coins_number, all_coins_number))
        return render_template('catalogs.html', catalog_list=catalog_list_new, watch_mode=True, add_mode=False,
                               groups=groups, current_group=current_group)

    @app.route('/statistics', methods=['POST', 'GET'])
    def statistics(data=None):
        all_collection = str(request.args.get('all_collection'))
        group = str(request.args.get('group'))
        catalog_id = request.args.get('catalog_id')
        print(group, catalog_id, all_collection)
        parametr = request.form.get('sort')
        if parametr == None:
            parametr = 'country'
        coins_list=[]

        if (group == 'None') and (catalog_id == None): ### проверки независимые, можно сделать эффективнее
            all_collection = 'True'

        if all_collection == 'True':
            coins_list = list(unique_everseen([coin for coin in db.session.query(Coin) for user_coin_row in coin.user_coins
                                               if (user_coin_row.user_id == current_user.id) and not (coin.coin_id == 1)]))
        if group != 'None':
            coins_list = []
            catalogs = db.session.query(Catalog).filter(Catalog.group == group).all()
            for current_catalog in catalogs:
                coins_list += [db.session.query(Coin).filter(Coin.coin_id == row.coin_id).first() for row in
                              current_catalog.user_coins if not (row.coin_id == 1)]
            coins_list = list(set(coins_list))

        if catalog_id != None:
            current_catalog = db.session.query(Catalog).filter(Catalog.catalog_id == catalog_id).first()
            coins_list = [db.session.query(Coin).filter(Coin.coin_id == row.coin_id).first() for row in current_catalog.user_coins if not (row.coin_id == 1)]
            catalog_name = db.session.query(Catalog).filter(Catalog.catalog_id == catalog_id).first().catalog_name
        else:
            catalog_name = None

        groups = list(
            unique_everseen(
                [catalog.group for catalog in db.session.query(Catalog) for user_coin_row in catalog.user_coins
                 if (user_coin_row.user_id == current_user.id) and not (catalog.catalog_id == 1)]))

        catalog_list = list(
            unique_everseen([catalog for catalog in db.session.query(Catalog) for user_coin_row in catalog.user_coins
                             if (user_coin_row.user_id == current_user.id) and not (catalog.catalog_id == 1)]))
        if not (parametr == None):
            types = [getattr(coin, str(parametr)) for coin in coins_list]
            total_price = 0
            for coin in coins_list:
                total_price += coin.price*db.session.query(CoinNumber).filter_by(user_id = current_user.id, coin_id = coin.coin_id).first().number
            h = Counter(types)
        else:
            h = {'':''} ###
        num = len(coins_list)
        print(h)

        if (group == 'None') and (catalog_id == None):
            all_collection = True
            parametr = 'country'

        return render_template('statistics.html',
                               my_list=h,
                               parametr=parametr,
                               groups=groups,
                               catalog_list=catalog_list,
                               all_collection=all_collection,
                               group=group,
                               catalog_id=catalog_id,
                               catalog_name= catalog_name,
                               total_price = round(total_price),
                               num=num)


    @app.route('/coin_information/', methods=['POST', 'GET'])
    def coin_information():
        print('coin information')
        coin_id = request.form.get('coin_id')
        coin = (db.session.query(Coin).filter_by(coin_id=int(coin_id)).first())
        return render_template('coin_information.html', coin=coin)

    @app.route('/catalog/', methods=['POST', 'GET'])
    def catalog():
        print('catalog')
        data = dict(catalog_id=request.args.get('catalog_id'))
        catalog_id = request.form.get('catalog_id')
        if catalog_id == None:
            catalog_id = data['catalog_id']
        add_list = request.form.getlist('check')
        print(add_list)
        if add_list != []:
            for add_coin_id in add_list:
                add_coin_id = int(add_coin_id)
                db.session.add(UserCoin(user=current_user,
                                        coin=db.session.query(Coin).filter_by(coin_id=add_coin_id).first(),
                                        catalog=db.session.query(Catalog).filter_by(catalog_id=add_catalog_id).first()))
                db.session.commit()
                print('Добавлена')

        catalog_name = db.session.query(Catalog).filter(Catalog.catalog_id == catalog_id).first().catalog_name ##### смотри на сторочку ниже
        current_catalog = db.session.query(Catalog).filter(Catalog.catalog_id == catalog_id).first()
        catalog_coins_list = [db.session.query(Coin).filter(Coin.coin_id == row.coin_id).first() for row in current_catalog.user_coins if not (row.coin_id == 1)]
        coins_list_new = []
        for coin in catalog_coins_list:
            number = db.session.query(CoinNumber).filter(CoinNumber.coin_id == coin.coin_id,
                                                         CoinNumber.user_id == current_user.id).first()
            coins_list_new.append((coin, number))
        group = Catalog.query.filter_by(catalog_id = catalog_id).first().group
        return render_template('catalog.html', catalog_coins_list=coins_list_new,
                               catalog_name=catalog_name, catalog_id=catalog_id,
                               group = group)  ## может id мщжно и не передавать а передать весть каталог?

    @app.route('/add_coins/', methods=['POST'])
    def add_coins():
        print('add_coins')
        catalog_id = request.form.get('catalog_id')
        if not (request.form.get('catalog_id') == None):
            global add_catalog_id ### зачем мне это?
            add_catalog_id = int(request.form.get('catalog_id'))
        coins_list = list(unique_everseen([coin for coin in db.session.query(Coin)
                                                for user_coin_row in coin.user_coins
                                                if (user_coin_row.user_id == current_user.id) and not (coin.coin_id == 1)]))
        current_catalog = db.session.query(Catalog).filter(Catalog.catalog_id == catalog_id).first()
        catalog_coins_list = [db.session.query(Coin).filter(Coin.coin_id == row.coin_id).first() for row in
                              current_catalog.user_coins if not (row.coin_id == 1)]

        sort_type = str(request.form.get('sort'))
        if sort_type == 'None':
            sort_type = 'Без сортировки'

        if sort_type == 'по возрастанию цены':  ####
            coins_list = sorted(coins_list, key=attrgetter('price'))
        elif sort_type == 'по убыванию цены':
            coins_list = sorted(coins_list, key=attrgetter('price'), reverse=True)
        elif sort_type == 'по возрастанию года':
            coins_list = sorted(coins_list, key=attrgetter('year'))
        elif sort_type == 'по убыванию года':
            coins_list = sorted(coins_list, key=attrgetter('year'), reverse=True)
        elif sort_type == 'по стране':
            coins_list = sorted(coins_list, key=attrgetter('country'))
        elif sort_type == 'по весу':
            coins_list = sorted(coins_list, key=attrgetter('weight'))  ###
        elif sort_type == 'по диаметру':  ###чекнуть английский
            coins_list = sorted(coins_list, key=attrgetter('diameter'))  ###
        elif sort_type == 'по каталожному номеру':
            coins_list = sorted(coins_list, key=attrgetter('krause_number'))
        elif sort_type == 'по периоду':
            coins_list = sorted(coins_list, key=attrgetter('period'))
        else:
            pass
        coins_list_new = [coin for coin in coins_list if not coin in catalog_coins_list]
        print(catalog_id)
        return render_template('add_coins.html',
                               coins_list=coins_list_new,
                               sort_type=sort_type,
                               change_number_mode=False,
                               catalog_id=catalog_id)

    @app.route('/delete_coin/', methods=['POST', 'GET'])
    def delete_coin():
        print('delete coin')
        coin_id = int(request.form.get('coin_id'))
        coins_to_del = (db.session.query(UserCoin).filter_by(coin_id=coin_id, user_id=current_user.id).all())
        coins_to_del_from_numbers = (db.session.query(CoinNumber).filter_by(coin_id=coin_id, user_id=current_user.id).first())
        for coin_to_del in coins_to_del:
            delete(coin_to_del)
            delete(coins_to_del_from_numbers)
        return redirect(url_for('gallery'))

    @app.route('/delete_catalog/', methods=['POST', 'GET'])
    def delete_catalog():
        print('delite catalog')
        catalog_id = int(request.form.get('catalog_id_to_delete'))
        catalogs_to_del = db.session.query(UserCoin).filter_by(catalog_id=catalog_id, user_id=current_user.id).all()
        cat_del = db.session.query(Catalog).filter_by(catalog_id=catalog_id).first()
        for catalog_to_del in catalogs_to_del:
            delete(catalog_to_del)  # из списка отношений
        delete(cat_del)  ##удаление из списка каталогов
        return redirect(url_for('catalogs'))

    @app.route('/delete_coin_from_catalog/', methods=['POST', 'GET'])
    def delete_coin_from_catalog():
        print('delete coin from catalog')
        catalog_id = int(request.form.get('catalog_id_from_del'))
        delete(db.session.query(UserCoin).filter_by(
            coin_id=int(request.form.get('coin_id_to_del')),
            user_id=current_user.id,
            catalog_id=catalog_id).first())
        return redirect(url_for('catalog', catalog_id = catalog_id))

    @app.route('/change_number/', methods=['POST', 'GET'])
    def change_number():
        print('change number')
        if request.form.get('coin_id_to_change_number') is not None:
            coin_id_to_change_number = request.form.get('coin_id_to_change_number')
        else:
            coin_id_to_change_number = request.form.get('coin_id_to_change_number_modal')
            CoinNumber.query.filter_by(user_id=current_user.id, coin_id = coin_id_to_change_number ).first().number = request.form.get('num')
            db.session.commit()
            return redirect(url_for('gallery'))

        try:
            previous_number = CoinNumber.query.filter_by(user_id=current_user.id, coin_id = coin_id_to_change_number ).first().number
        except:
            previous_number = 0
        name_of_changing_coin = Coin.query.filter_by(coin_id = coin_id_to_change_number).first().name
        coins_list = list(unique_everseen([coin for coin in db.session.query(Coin) for user_coin_row in coin.user_coins
                                                if (user_coin_row.user_id == current_user.id) and not (coin.coin_id == 1)]))
        coins_list_new = []
        for coin in coins_list:
            number = db.session.query(CoinNumber).filter(CoinNumber.coin_id == coin.coin_id, CoinNumber.user_id == current_user.id).first()
            coins_list_new.append((coin, number))

        return render_template('gallery.html', change_number_mode = True, coins_list=coins_list_new,
                               coin_id_to_change_number=coin_id_to_change_number,
                               name_of_changing_coin=name_of_changing_coin,
                               previous_number = previous_number)

    @app.route('/rename_catalog/', methods=['POST', 'GET'])
    def rename_catalog():
        print('rename_catalog')
        catalog_id = request.form.get('catalog_id_to_rename')
        new_name = request.form.get('new_name')
        Catalog.query.filter_by(catalog_id = catalog_id).first().catalog_name = new_name
        db.session.commit()
        return redirect(url_for('catalog', catalog_id=catalog_id))

    @app.route('/rename_group/', methods=['POST', 'GET'])
    def rename_group():
        print('rename_group')
        current_group = request.form.get('group_to_rename')
        print(current_group)
        new_name = request.form.get('new_name')
        print(new_name)
        print(current_group, new_name)
        for catalog in list(set([Catalog.query.filter_by(catalog_id = row.catalog_id).first() for row in db.session.query(UserCoin).filter_by(user_id = current_user.id).all()])):
            print(catalog.catalog_name)   ##### при большом кол-ве монет будет очень плохо. нужно добавлять user_id каталогу
            if catalog.group == current_group:
                Catalog.query.filter_by(catalog_id=catalog.catalog_id).first().group = new_name
                db.session.commit()
        current_group = new_name
        return redirect(url_for('catalogs',current_group=current_group))

