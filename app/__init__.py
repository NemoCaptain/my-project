import flask

from .api import api_views
from .models import db, auth, User

from .views import views
from .login_views import login_views

app = flask.Flask(__name__, static_url_path='/static', static_folder='static')


app.config['SECRET_KEY'] = '9OLWxcD4583k4K6iuopO'
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///./db.sqlite'
app.config["CACHE_TYPE"] = "null"

auth.init_app(app)
db.init_app(app)
with app.app_context():
    db.create_all()

api_views(app)
login_views(app)
views(app)


@auth.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))
