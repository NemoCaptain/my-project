import requests
from bs4 import BeautifulSoup
from .models import Coin, db, UserCoin, Catalog, CoinNumber
from flask_login import current_user
import random
import time

headers = {
    "User-agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.80 Safari/537.36"}

# получение полной информации про каждую монету
def get_all_information(coins_links):
    user_agents = [
        "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.80 Safari/537.36"
    ]

    for coin_link in coins_links:

        link = coin_link

        # подготовка ссылки на монету для поиска данных
        r = requests.get(coin_link, headers = {
             "User-agent": random.choice(user_agents)})
        r = r.text
        soup = BeautifulSoup(r, 'lxml')

        # поиск названия монеты
        try:
            name = soup.find('div', style='clear:both;').find('h1').text
        except:
            name = ''

        # поиск сссылки на изображение аверса и реверса монеты
        try:
            obverse_link = soup.find('table', class_='coin-img').find_all('a')[0].get('href')
        except:
            obverse_link = ''

        try:
            reverse_link = soup.find('table', class_='coin-img').find_all('a')[1].get('href')
        except:
            reverse_link = ''

        # поиск цены (иначе цена == 0)
        try:
            price = soup.find('a', class_='gray-12 right pricewj').find('span').text.replace(',', '')
        except:
            price = '0.00'

        # внесение всей информации про монету из таблиек под ней
        try:
            for tr in (soup.find_all('table', class_='tbl coin-info')[0]).find_all('tr'):
                if tr.find('th').text =='Страна':
                    country = tr.find('td').text
        except:
            country='?'

        try:
            for tr in (soup.find_all('table', class_='tbl coin-info')[0]).find_all('tr'):
                if tr.find('th').text =='Номинал':
                    denomination = tr.find('td').text
        except:
            denomination='?'

        try:
            for tr in (soup.find_all('table', class_='tbl coin-info')[0]).find_all('tr'):
                if tr.find('th').text =='Год':
                    year = tr.find('td').text
        except:
            year='?'

        try:
            for tr in (soup.find_all('table', class_='tbl coin-info')[0]).find_all('tr'):
                if tr.find('th').text =='Период':
                    period = tr.find('td').text
        except:
            period='?'

        try:
            for tr in (soup.find_all('table', class_='tbl coin-info')[0]).find_all('tr'):
                if tr.find('th').text =='Вид чекана':
                    coin_type = tr.find('td').text
        except:
            coin_type='?'

        try:
            for tr in (soup.find_all('table', class_='tbl coin-info')[0]).find_all('tr'):
                if tr.find('th').text =='Форма':
                    shape = tr.find('td').text
        except:
            shape='?'

        try:
            for tr in (soup.find_all('table', class_='tbl coin-info')[0]).find_all('tr'):
                if tr.find('th').text =='Вес (г)':
                    weight = tr.find('td').text
        except:
            weight='?'

        try:
            for tr in (soup.find_all('table', class_='tbl coin-info')[0]).find_all('tr'):
                if tr.find('th').text =='Диаметр (мм)':
                     diameter= tr.find('td').text
        except:
            diameter='?'

        try:
            for tr in (soup.find_all('table', class_='tbl coin-info')[0]).find_all('tr'):
                if tr.find('th').text =='Гурт':
                    edge = tr.find('td').text
        except:
            edge='?'

        try:
            for tr in (soup.find_all('table', class_='tbl coin-info')[0]).find_all('tr'):
                if tr.find('th').text =='Толщина (мм)':
                    thickness = tr.find('td').text
        except:
            thickness='?'

        try:
            for tr in (soup.find_all('table', class_='tbl coin-info')[0]).find_all('tr'):
                if tr.find('th').text =='Отношение авс/рев':
                    alignment = tr.find('td').text
        except:
            alignment='?'

        try:
            for tr in (soup.find_all('table', class_='tbl coin-info')[0]).find_all('tr'):
                if tr.find('th').text =='Материал':
                    composition = tr.find('td').text
        except:
            composition='?'

        try:
            for tr in (soup.find_all('table', class_='tbl coin-info')[0]).find_all('tr'):
                if tr.find('th').text =='WWC номер':
                     krause_number= tr.find('td').text
        except:
            krause_number='?'

        try:
            for tr in (soup.find_all('table', class_='tbl coin-info')[1]).find_all('tr'):
                if tr.find('th').text =='Аверс':
                     reverse= tr.find('td').text
        except:
            reverse='?'

        try:
            for tr in (soup.find_all('table', class_='tbl coin-info')[1]).find_all('tr'):
                if tr.find('th').text =='Реверс':
                     obverse= tr.find('td').text
        except:
            obverse='?'

        '''print(link,'\n', name,'\n', price,'\n', obverse_link,'\n', reverse_link,'\n',
              krause_number,'\n', country,'\n', denomination, '\n',year,'\n', period,
              '\n', coin_type,'\n', composition, '\n',edge,'\n',shape,'\n', alignment,
              '\n', weight,'\n', diameter,'\n', thickness,'\n', obverse ,'\n', reverse )'''

        if (db.session.query(db.exists().where(Coin.link == link)).scalar() == True):
            coin_id = db.session.query(Coin).filter(Coin.link == link).first().coin_id##
            ids = []                                                                  ## чёт долго проверяет
            for row in current_user.user_coins:                                       ##
                ids.append(row.coin_id)                                               ##
            if not(coin_id in ids):
                coin = db.session.query(Coin).filter(Coin.link == link).first()
                user = current_user
                catalog = db.session.query(Catalog).filter(Catalog.catalog_id == 1).first()
                assoc1 = UserCoin(user=user, coin=coin, catalog=catalog)
                db.session.add(assoc1)
                db.session.commit()

                assoc2 = CoinNumber(user=user, coin=coin, number=1)
                db.session.add(assoc2)
                db.session.commit()
        else:
            coin = Coin(link=link, name=name ,price=price ,obverse_link=obverse_link, reverse_link=reverse_link,
            krause_number=krause_number, country=country ,denomination=denomination, year=year,
            period=period,coin_type=coin_type ,composition=composition,edge=edge,shape=shape,
            alignment=alignment,weight=weight ,diameter=diameter,thickness=thickness ,obverse=obverse,
            reverse=reverse)
            db.session.add(coin)
            db.session.commit()

            user = current_user
            catalog = db.session.query(Catalog).filter(Catalog.catalog_id == 1).first()
            assoc1 = UserCoin(user=user, coin=coin, catalog=catalog)
            db.session.add(assoc1)
            db.session.commit()

            assoc2 = CoinNumber(user=user, coin=coin, number=1)
            db.session.add(assoc2)
            db.session.commit()

            id = coin.coin_id
            img_obverse = requests.get(obverse_link, headers=headers).content
            img_reverse = requests.get(reverse_link, headers=headers).content
            with open('app/static/obverses/obverse_' + str(id) + '.jpg', 'wb') as handler:
                handler.write(img_obverse)
            with open('app/static/reverses/reverse_' + str(id) + '.jpg', 'wb') as handler:
                handler.write(img_reverse)
    time.sleep(2)
def get_coins_links(url):
    user_agents = [
        "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.80 Safari/537.36"
    ]
    coins_links = []
    r0 = requests.get(url, headers = {
             "User-agent": random.choice(user_agents)})
    r0 = r0.text
    soup = BeautifulSoup(r0, 'lxml')

    # подсчёт количества страциц в галлерее монет
    try:
        pages_number = int((soup.find('div', class_='pages').find_all('a')[-1].text))
    except:
        pages_number = 1
    '''pages_number = 1'''
    # копируем каждую ссылку на монету на каждой страцине в галлерее
    # и записываем в 'coinns_links'
    for page in range(pages_number):
        r = requests.get(url + '&page=' + str(page + 1), headers = {
             "User-agent": random.choice(user_agents)})
        r = r.text
        soup = BeautifulSoup(r, 'lxml')
        coins_classes = soup.find_all('div', class_='coin')
        for coin_class in coins_classes:
            coins_links.append('https://ru.ucoin.net' + coin_class.find('a', class_='blue-15').get('href'))
            time.sleep(2)
        print('page', page + 1, 'from', pages_number)

    # возвращаем строку 'coins_links' с ссылками на страныцы монет
    return coins_links

