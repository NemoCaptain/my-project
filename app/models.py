import os
import random

from flask_login import LoginManager, AnonymousUserMixin
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()
auth = LoginManager()
auth.login_view = 'login'

class UserCoin(db.Model):
    __tablename__ = 'user_coins'

    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey("user.id"), nullable=False)
    coin_id = db.Column(db.Integer, db.ForeignKey("coin.coin_id"), nullable=False)
    catalog_id = db.Column(db.Integer, db.ForeignKey("catalog.catalog_id"), nullable=True)

    __table_args__ = (db.UniqueConstraint(user_id, coin_id, catalog_id),)

    user = db.relationship("User", back_populates="user_coins")
    coin = db.relationship("Coin", back_populates="user_coins")
    catalog = db.relationship("Catalog", back_populates="user_coins")

class CoinNumber(db.Model):
    __tablename__ = 'coin_number'

    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey("user.id"), nullable=False)
    coin_id = db.Column(db.Integer, db.ForeignKey("coin.coin_id"), nullable=False)
    number = db.Column(db.Integer,  nullable=True)

    __table_args__ = (db.UniqueConstraint(user_id, coin_id, number),)

    user = db.relationship("User", back_populates="coin_number")
    coin = db.relationship("Coin", back_populates="coin_number")

class User(db.Model):
    __tablename__ = 'user'

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(100), unique=True)
    password = db.Column(db.String(100))
    name = db.Column(db.String(1000))
    is_active = True
    user_coins = db.relationship("UserCoin", back_populates="user")
    coin_number = db.relationship("CoinNumber", back_populates="user")

    def is_authenticated(self):
        """Return True if the user is authenticated."""
        return self.authenticated

    def get_id(self):
        return self.id


class Coin(db.Model):
    __tablename__ = 'coin'
    coin_id = db.Column(db.Integer, primary_key=True)
    link = db.Column(db.String(100), unique=True)
    name = db.Column(db.String(100))
    price = db.Column(db.Integer)
    obverse_link = db.Column(db.String(100))
    reverse_link = db.Column(db.String(100))
    krause_number = db.Column(db.String(100))
    country = db.Column(db.String(100))
    denomination = db.Column(db.String(100))
    year = db.Column(db.String(100))
    period = db.Column(db.String(100))
    coin_type = db.Column(db.String(100))
    composition = db.Column(db.String(100))
    edge = db.Column(db.String(100))
    shape = db.Column(db.String(100))
    alignment = db.Column(db.String(100))
    weight = db.Column(db.String(100))
    diameter = db.Column(db.String(100))
    thickness = db.Column(db.String(100))
    obverse = db.Column(db.String(100))
    reverse = db.Column(db.String(100))
    user_coins = db.relationship("UserCoin", back_populates="coin")
    coin_number = db.relationship("CoinNumber", back_populates="coin")
    is_active = True

class Catalog(db.Model):
    __tablename__ = 'catalog'
    catalog_id = db.Column(db.Integer, primary_key=True)
    catalog_name = db.Column(db.String(100))
    group = db.Column(db.String(100))
    user_coins = db.relationship("UserCoin", back_populates="catalog")
    is_active = True


