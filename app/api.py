import json
import flask

from .models import User


def api_views(app: flask.Flask):
    @app.route('/api/is_login_available')
    def is_login_available():
        return str(bool(User.query.filter_by(username=flask.request.args.get('username')).first()))
