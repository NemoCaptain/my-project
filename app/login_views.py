from flask_login import login_user, logout_user, login_required, current_user
from flask import request, render_template, redirect
from werkzeug.security import generate_password_hash, check_password_hash

from .models import User, db

def login_views(app):
    @app.route('/login', methods=['POST', 'GET'])
    def login():
        if request.method == 'GET':
            return render_template('login.html', wrong_login=False)
        else:
            user = User.query.filter_by(username=request.form.get('username')).first()
            if user and check_password_hash(user.password, request.form.get('password')):
                login_user(user)
                return redirect(request.args.get('next', '/gallery'))
            else:
                return render_template('login.html', wrong_login=True)

    @app.route('/register', methods=['POST', 'GET'])
    def register():
        if request.method == 'GET':
            return render_template('register.html', wrong_login=False)
        else:
            if not User.query.filter_by(username=request.form.get('username')).first():
                user = User(name=request.form.get('full_name'), username=request.form.get('username'),
                            password=generate_password_hash(request.form.get('password'), method='sha256'))
                db.session.add(user)
                db.session.commit()
                login_user(user)
                return redirect(request.args.get('next', '/gallery'))
            else:
                return render_template('register.html', wrong_login=True)

    @app.route('/settings', methods=['POST', 'GET'])
    @login_required
    def settings():
        if request.method == 'GET':
            return render_template('settings.html')
        else:
            current_user.name = request.form.get('name')
            if request.form.get('password'):
                current_user.password = generate_password_hash(request.form.get('password'), method='sha256')
            db.session.commit()
            return redirect(request.args.get('next', '/gallery'))

    @app.route('/logout')
    @login_required
    def logout():
        logout_user()
        return redirect('/')
